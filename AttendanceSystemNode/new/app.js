var express = require("express");
var app = express();

mongoose.connect("mongodb://localhost/AttendanceSystem", function(err) {
  if (err) {
    console.log(err);
  } else {
    console.log("connected");
  }
});

const productRoutes = require("./api/routes/product");

app.use("/product", productRoutes);

const userRoutes = require("./api/routes/user");

app.use("/user", userRoutes);
app.listen(3000, function(err) {
  if (err) {
    console.log(err);
  } else {
    console.log("Listening");
  }
});
