import os
import cv2
from align_custom import AlignCustom
from face_feature import FaceFeature
from mtcnn_detect import MTCNNDetect
from tf_graph import FaceRecGraph
from matplotlib.image import imread
import argparse
import sys
from imutils.video import WebcamVideoStream
from imutils.video import FPS
from imutils import face_utils
import json
import dlib
import time
import glob
import numpy as np
import threading
f = open('./facerec_128D.txt', 'r')
data_set = json.loads(f.read())
FRGraph = FaceRecGraph()
MTCNNGraph = FaceRecGraph()
aligner = AlignCustom()
extract_feature = FaceFeature(FRGraph)
# scale_factor, rescales image for faster detection
face_detect = MTCNNDetect(MTCNNGraph, scale_factor=2)
vs = cv2.VideoCapture(0)


def findPerson(features, position, thres=0.6, percent_thres=55):
    result = "Unknown"
    smallest = sys.maxsize
    result = None
    for person in data_set.keys():
        person_data = data_set[person][position]
        for data in person_data:
            distance = np.sqrt(np.sum(np.square(data-features)))
            if distance < smallest:
                smallest = distance
                result = person
    percentage = min(100, 100 * thres / smallest)
    if percentage <= percent_thres:
        result = "Unknown"
    result = (result, percentage)
    return result


frameCounter = 0
currentFaceID = 0

# Variables holding the correlation trackers and the name per faceid
faceTrackers = {}
faceNames = {}
number_of_faces = 0

while True:
    _, frame = vs.read()
    rects, landmarks = face_detect.detect_face(frame, 30)
    for (i, rect) in enumerate(rects):
        cv2.rectangle(frame, (rect[0], rect[1]),
                      (rect[2], rect[3]), (0, 0, 255))
        aligned_face, face_pos = aligner.align(160, frame, landmarks[:, i])
        cv2.imshow("Aligned Frame", aligned_face)
        features_arr = extract_feature.get_feature(aligned_face)
        recog_data = findPerson(features_arr, face_pos)
        print(recog_data)
