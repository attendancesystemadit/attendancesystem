import os
import cv2
from align_custom import AlignCustom
from face_feature import FaceFeature
from mtcnn_detect import MTCNNDetect
from tf_graph import FaceRecGraph
import recognizePeople
from matplotlib.image import imread
import argparse
import sys
from imutils.video import FPS
import json
import time
import glob
import numpy as np
from threading import Thread
import filetype
from multiprocessing import Process

TIMEOUT = 10  # 10 seconds
fps = FPS().start()
f = open('./facerec_128D.txt', 'r')
data_set = json.loads(f.read())
vs = cv2.VideoCapture(0)


def frames_recognition(fram):

    try:
        kind = filetype.guess(fram)
        frame = cv2.imread(fram)
    except ValueError:
        frame = fram
    # print("[INFO] camera sensor warming up...")
    # get input from webcam
    detect_time = time.time()
    # frame = cv2.imread(_frame)
    # u can certainly add a roi here but for the sake of a demo i'll just leave it as simple as this
    rects, landmarks = face_detect.detect_face(
        frame, 30)  # min face size is set to 80x80
    aligns = []
    positions = []
    for (i, rect) in enumerate(rects):
        aligned_face, face_pos = aligner.align(160, frame, landmarks[:, i])
        if len(aligned_face) == 160 and len(aligned_face[0]) == 160:
            aligns.append(aligned_face)
            positions.append(face_pos)
        else:
            print("Align face failed")  # logc
        if len(aligns) > 0:
            thread = Process(target=recognizeProcess, args=(
                aligns, positions, rects, frame))
            thread.start()
            thread.join()
        # cv2.imshow("Frame", frame)
        # key = cv2.waitKey(1) & 0xFF
        # if key == ord("q") or key == ord("Q") or key == 27:
        #     break


def findPeople(features_arr, positions, thres=0.6, percent_thres=50):
    '''
    :param percent_thres: Minimum percentage for recognition
    :param features_arr: a list of 128d Features of all faces on screen
    :param positions: a list of face position types of all faces on screen
    :param thres: distance threshold
    :return: person name and percentage
    '''

    returnRes = []
    for (i, features_128D) in enumerate(features_arr):
        result = "Unknown"
        smallest = sys.maxsize
        for person in data_set.keys():
            person_data = data_set[person][positions[i]]
            for data in person_data:
                distance = np.sqrt(np.sum(np.square(data-features_128D)))
                if distance < smallest:
                    smallest = distance
                    result = person
        percentage = min(100, 100 * thres / smallest)
        if percentage <= percent_thres:
            result = "Unknown"
        returnRes.append((result, percentage))
    return returnRes


def receivingFrames():
    while(True):
        _, frame = vs.read()
        frames_recognition(frame)
        # time.sleep(2)


def recognizeProcess(aligns, positions, rects, frame):
    from align_custom import AlignCustom
    from face_feature import FaceFeature
    from mtcnn_detect import MTCNNDetect
    from tf_graph import FaceRecGraph
    FRGraph = FaceRecGraph()
    MTCNNGraph = FaceRecGraph()
    aligner = AlignCustom()
    extract_feature = FaceFeature(FRGraph)
    # scale_factor, rescales image for faster detection
    face_detect = MTCNNDetect(MTCNNGraph, scale_factor=3)
    people_list = []
    features_arr = extract_feature.get_features(aligns)
    recog_data = findPeople(features_arr, positions)
    for (i, rect) in enumerate(rects):
        people_list.append(recog_data[i][0])
    print(people_list)


def main(args):
    receivingFrames()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--mode", type=str,
                        help="Run camera recognition", default="camera")
    args = parser.parse_args(sys.argv[1:])
    FRGraph = FaceRecGraph()
    MTCNNGraph = FaceRecGraph()
    aligner = AlignCustom()
    extract_feature = FaceFeature(FRGraph)
    # scale_factor, rescales image for faster detection
    face_detect = MTCNNDetect(MTCNNGraph, scale_factor=3)
    main(args)
