#!/usr/bin/python
'''
    Author: Guido Diepen <gdiepen@deloitte.nl>
'''
# Import the OpenCV and dlib libraries
import threading
import time
import sys
import json
import requests
import cv2
import numpy as np
import dlib
# from imutils.video import WebcamVideoStream
from align_custom import AlignCustom
from face_feature import FaceFeature
from mtcnn_detect import MTCNNDetect
from tf_graph import FaceRecGraph
FRGraph = FaceRecGraph()
MTCNNGraph = FaceRecGraph()
aligner = AlignCustom()
extract_feature = FaceFeature(FRGraph)

f = open('./facerec_128D.txt', 'r')
data_set = json.loads(f.read())

# Initialize a face cascade using the frontal face haar cascade provided with
# the OpenCV library
# Make sure that you copy this file from the opencv project to the root of this
# project folder]
faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

# The deisred output width and height
OUTPUT_SIZE_WIDTH = 775
OUTPUT_SIZE_HEIGHT = 600

face_trackers = {}
face_names = []
# We are not doing really face recognition
t_end = time.time() + 40


def doRecognizePerson(i, positions, aligns):
    # print("[+] Recognizing Process")
    # position = face_trackers[i]["position"]
    # aligned_face = face_trackers[i]["face"]
    features_arr = extract_feature.get_feature(aligns)
    recog_data = findPerson(features_arr, positions)
    # face_trackers[i]["name"] = recog_data[0]
    if recog_data[0] == "Unknown":
        # face_trackers[i]["name"] = "Detecting"
        return "Detecting"
    else:
        if recog_data[0] in face_trackers[i]["count"]:
            face_trackers[i]["count"][recog_data[0]] += 1
            if face_trackers[i]["count"][recog_data[0]] > 5:
                face_trackers[i]["name"] = recog_data[0]
                set_name(recog_data[0])
        else:
            face_trackers[i]["count"][recog_data[0]] = 1
    # print(recog_data[0])
    # return recog_data[0]


def detectAndTrackMultipleFaces():
    # Open the first webcame device
    capture = cv2.VideoCapture(0)

    # Create two opencv named windows
    cv2.namedWindow("base-image", cv2.WINDOW_AUTOSIZE)
    cv2.namedWindow("result-image", cv2.WINDOW_AUTOSIZE)

    # Position the windows next to eachother
    cv2.moveWindow("base-image", 0, 100)
    cv2.moveWindow("result-image", 400, 100)

    # Start the window thread for the two windows we are using
    cv2.startWindowThread()

    # The color of the rectangle we draw around the face
    rectangleColor = (0, 165, 255)

    # variables holding the current frame number and the current faceid
    frameCounter = 0
    currentFaceID = 0

    # Variables holding the correlation trackers and the name per faceid
    # faceNames = {}
    # number_of_faces = 0
    try:
        while time.time() < t_end:
            # Retrieve the latest image from the webcam
            _, frame = capture.read()

            # Resize the image to 320x240
            result = frame.copy()

            # Check if a key was pressed and if it was Q, then break
            # from the infinite loop
            pressedKey = cv2.waitKey(2)
            if pressedKey == ord('Q'):
                break

            # Result image is the image we will show the user, which is a
            # combination of the original image from the webcam and the
            # overlayed rectangle for the largest face

            # STEPS:
            # * Update all trackers and remove the ones that are not
            #   relevant anymore
            # * Every 10 frames:
            #       + Use face detection on the current frame and look
            #         for faces.
            #       + For each found face, check if centerpoint is within
            #         existing tracked box. If so, nothing to do
            #       + If centerpoint is NOT in existing tracked box, then
            #         we add a new tracker with a new face-id

            # Increase the framecounter
            frameCounter += 1
            # Update all the trackers and remove the ones for which the update
            # indicated the quality was not good enough
            fidsToDelete = []
            for fid in face_trackers.keys():
                tracking_quality = face_trackers[fid]["tracker"].update(frame)

                # If the tracking quality isn't good enough, we must delete
                # this tracker
                if tracking_quality < 3:
                    fidsToDelete.append(fid)

            for fid in fidsToDelete:
                print("Removing fid " + str(fid) + " from list of trackers")
                face_trackers.pop(fid, None)

            # Every 10 frames, we will have to determine which faces
            # are present in the frame
            if (frameCounter % 10) == 0:
                aligns = []
                positions = []
                # For the face detection, we need to make use of a gray
                # colored image so we will convert the frame to a
                # gray-based image
                # Now use the haar cascade detector to find all faces
                # in the image
                rects, landmarks = face_detect.detect_face(
                    frame, 30)
                aligned_face = None
                position = None
                number_of_faces = len(rects)

                # Loop over all faces and check if the area for this
                # face is the largest so far
                # We need to convert it to int here because of the
                # requirement of the dlib tracker. If we omit the cast to
                # int here, you will get cast errors since the detector
                # returns numpy.int32 and the tracker requires an int
                for (i, rect) in enumerate(rects):
                    aligned_face, face_pos = aligner.align(
                        160, frame, landmarks[:, i])
                    if len(aligned_face) == 160 and len(aligned_face[0]) == 160:
                        aligns.append(aligned_face)
                        position = face_pos
                        positions.append(face_pos)
                    else:
                        print("Align face failed")
                    x = rect[0]
                    y = rect[1]
                    w = rect[2]-x
                    h = rect[3]-y

                    # calculate the centerpoint
                    x_bar = x + 0.5 * w
                    y_bar = y + 0.5 * h

                    # Variable holding information which faceid we
                    # matched with
                    matchedFid = None

                    # Now loop over all the trackers and check if the
                    # centerpoint of the face is within the box of a
                    # tracker
                    for fid in face_trackers.keys():

                        tracked_position = face_trackers[fid]["tracker"].get_position(
                        )

                        t_x = int(tracked_position.left())
                        t_y = int(tracked_position.top())
                        t_w = int(tracked_position.width())
                        t_h = int(tracked_position.height())
                        # calculate the centerpoint
                        t_x_bar = t_x + 0.5 * t_w
                        t_y_bar = t_y + 0.5 * t_h

                        # check if the centerpoint of the face is within the
                        # rectangleof a tracker region. Also, the centerpoint
                        # of the tracker region must be within the region
                        # detected as a face. If both of these conditions hold
                        # we have a match
                        if ((t_x <= x_bar <= (t_x + t_w)) and
                            (t_y <= y_bar <= (t_y + t_h)) and
                            (x <= t_x_bar <= (x + w)) and
                                (y <= t_y_bar <= (y + h))):
                            matchedFid = i
                    # If no matched fid, then we have to create a new tracker
                    if matchedFid is None:

                        print("Creating new tracker " + str(currentFaceID))

                        # Create and store the tracker
                        tracker = dlib.correlation_tracker()
                        tracker.start_track(frame,
                                            dlib.rectangle(x-10,
                                                           y-20,
                                                           x+w+10,
                                                           y+h+20))
                        face_trackers[i] = {
                            "tracker": tracker, "face": aligned_face, "position": position, "name": "Detecting", "count": {}}
                        # Start a new thread that is used to simulate
                        # face recognition. This is not yet implemented in this
                        # version :)
                        # if(len(rects) != 0):
                        try:
                            for i, index in zip(range(0, len(face_trackers.keys())),
                                                face_trackers.keys()):
                                face_trackers[i] = face_trackers.pop(index)
                        except:
                            pass
                            # Increase the currentFaceID counter
                        currentFaceID = len(rects)

            # Now loop over all the trackers we have and draw the rectangle
            # around the detected faces. If we 'know' the name for this person
            # (i.e. the recognition thread is finished), we print the name
            # of the person, otherwise the message indicating we are detecting
            # the name of the person
            for fid in face_trackers.keys():
                tracked_position = face_trackers[fid]["tracker"].get_position()
                t_x = int(tracked_position.left())
                t_y = int(tracked_position.top())
                t_w = int(tracked_position.width())
                t_h = int(tracked_position.height())

                cv2.rectangle(result, (t_x, t_y),
                              (t_x + t_w, t_y + t_h),
                              rectangleColor, 2)
                if fid in face_trackers.keys() and face_trackers[fid]["name"] != "Detecting":
                    cv2.putText(result, face_trackers[fid]["name"],
                                (int(t_x + t_w/2), int(t_y)),
                                cv2.FONT_HERSHEY_SIMPLEX,
                                0.5, (255, 255, 255), 2)
                else:
                    cv2.putText(result, "Detecting...",
                                (int(t_x + t_w/2), int(t_y)),
                                cv2.FONT_HERSHEY_SIMPLEX,
                                0.5, (255, 255, 255), 2)
                    if face_trackers[fid]["name"] == "Detecting":
                        cropped = frame[t_y:t_y+t_h, t_x:t_x+t_h]
                        # cv2.imshow("Cropped", cropped)
                        try:
                            rects, landmarks = face_detect.detect_face(
                                cropped, 30)
                        except:
                            print("Detection exception")
                        aligned_face = None
                        position = None
                        try:
                            for (i, rect) in enumerate(rects):
                                aligned_face, face_pos = aligner.align(
                                    160, cropped, landmarks[:, i])
                                if len(aligned_face) == 160 and len(aligned_face[0]) == 160:
                                    aligns.append(aligned_face)
                                    position = face_pos
                                    positions.append(face_pos)
                                else:
                                    print("Align face failed")
                            # face = face_trackers[i]
                            # t = threading.Thread(
                            #     target=doRecognizePerson, args=[fid, face_pos, aligned_face])
                            # t.start()
                            doRecognizePerson(
                                fid, face_pos, aligned_face)
                        except:
                            print("Alignment exception")

            # Since we want to show something larger on the screen than the
            # original 320x240, we resize the image again
            #
            # Note that it would also be possible to keep the large version
            # of the frame and make the result image a copy of this large
            # base image and use the scaling factor to draw the rectangle
            # at the right coordinates.
            # largeResult = cv2.resize(resultImage,
            #                          (OUTPUT_SIZE_WIDTH, OUTPUT_SIZE_HEIGHT))

            # Finally, we want to show the images on the screen
            # cv2.imshow("base-image", frame)
            cv2.imshow("result-image", result)

    # To ensure we can also deal with the user pressing Ctrl-C in the console
    # we have to check for the KeyboardInterrupt exception and break out of
    # the main loop
    except KeyboardInterrupt as e:
        pass
    # Destroy any OpenCV windows and exit the application
    cv2.destroyAllWindows()
    # exit(0)


def set_name(name):
    for key in face_names:
        if key == name:
            return

    face_names.append(name)


def findPeople(features_arr, positions, thres=0.6, percent_thres=70):
    '''
    :param percent_thres: Minimum percentage for recognition
    :param features_arr: a list of 128d Features of all faces on screen
    :param positions: a list of face position types of all faces on screen
    :param thres: distance threshold
    :return: person name and percentage
    '''
    returnRes = []
    for (i, features_128D) in enumerate(features_arr):
        result = "Unknown"
        smallest = sys.maxsize
        for person in data_set.keys():
            person_data = data_set[person][positions[i]]
            for data in person_data:
                distance = np.sqrt(np.sum(np.square(data-features_128D)))
                if distance < smallest:
                    smallest = distance
                    result = person
        percentage = min(100, 100 * thres / smallest)
        if percentage <= percent_thres:
            result = "Unknown"
        returnRes.append((result, percentage))
    return returnRes


def findPerson(features, position, thres=0.6, percent_thres=70):
    result = "Unknown"
    smallest = sys.maxsize
    result = None
    for person in data_set.keys():
        person_data = data_set[person][position]
        for data in person_data:
            distance = np.sqrt(np.sum(np.square(data-features)))
            if distance < smallest:
                smallest = distance
                result = person
    percentage = min(100, 100 * thres / smallest)
    if percentage <= percent_thres:
        result = "Unknown"
    result = (result, percentage)
    return result


if __name__ == '__main__':
    face_detect = MTCNNDetect(MTCNNGraph, scale_factor=2)
    detectAndTrackMultipleFaces()
    enroll = []
    temp = {}
    with open('enrollment.json') as f:
        data = json.load(f)
        for name in face_names:
            temp["Name"] = name
            temp["Enrollment"] = data[name]
            enroll.append(temp)
            temp = {}

        payLoad = dict()
        payLoad["data"] = enroll
        r = requests.post('http://192.168.137.1:3000/upload',
                          json=payLoad)
        print(enroll)
        # print(r)
